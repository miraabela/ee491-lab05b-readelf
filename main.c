///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 05 - readelf
///
/// @file main.c
/// @version 2.0
///
/// @author Mirabela Medallon <mirabela@hawaii.edu>
/// @brief  Lab 05 - readelf - EE 491F - Spr 2021
/// @date   March 5, 2021
///////////////////////////////////////////////////////////////////////////////

// NOTE: I did the extra credit.

#include <stdio.h>
#include "readelfbela.h"

#define MAX_FILE_COUNT 256
#define MAX_FILENAME_LEN 256

bool flagEH, flagSH, flagNone;
unsigned char flagXs, filescount;
char files[MAX_FILE_COUNT][MAX_FILENAME_LEN];
char specifiedSections[MAX_SECTIONS][MAX_SECTION_NAME_LEN];
char sectionNames[MAX_SECTIONS][MAX_SECTION_NAME_LEN];

bool checkValidFlagChar(char ch) {
   if (ch == 'h') {
      flagEH = true;
      flagNone = false;
      return true;
   }
   if (ch == 't') {
      flagSH = true;
      flagNone = false;
      return true;
   }
   if (ch == 'x') {
      flagNone = false;
      return true;
   }

   return false;
}

bool checkValidFlagWord(const char * str) {
   if (strcmp(str,"--file-header")==0) {
      flagEH = true;
      flagNone = false;
      return true;
   }
   if (strcmp(str,"--section-details")==0) {
      flagSH = true;
      flagNone = false;
      return true;
   }

   // if (strcmp(str,"--section-headers")==0) {
   //    flagSH = true;
   //    flagNone = false;
   //    return true;
   // }

   // if (strcmp(str,"--sections")==0) {
   //    flagSH = true;
   //    flagNone = false;
   //    return true;
   // }

   if (strncmp(str, "--hex-dump=", 11) == 0) {
      flagNone = false;
      return true;
   }

   return false;
}

void printUse() {
	printf("Usage: readelf <option(s)> elf-file(s)\n");
	printf(" Display information about the contents of ELF format files\n");
	printf(" Options are:\n");
	printf("  -h --file-header       Display the ELF file header\n");
   printf("  -t --section-details   Display the section details\n");
   printf("  -x --hex-dump=<number|name>\n");
   printf("                         Dump the contents of section <number|name> as bytes\n");
}

void checkInput(int argc, const char * argv[]) {
   flagNone = true;
   flagSH = flagEH = false;
   flagXs = filescount = 0;


   if (argc < 2) {
      printUse();
      exit(EXIT_FAILURE);
   }

   // Get options, get filename, get -x sections
   for (int i=1; i<argc; i++) {
      if (argv[i][0] == '-') {
         if (argv[i][1] == '-') {
            if (!checkValidFlagWord(argv[i])) {
               fprintf(stderr, "readelf: unrecognized option '%s'\n", argv[i]);
               printUse();
               exit(EXIT_FAILURE);
            } else if (strncmp(argv[i], "--hex-dump=", 11) == 0) {
               strcpy(specifiedSections[flagXs], argv[i]+11);
               flagXs++;
            }
         } else {
            int j = 1;
            while (argv[i][j] != '\0') {
               if (!checkValidFlagChar(argv[i][j])) {
                  fprintf(stderr, "readelf: invalid option -- '%c'\n", argv[i][j]);
                  printUse();
                  exit(EXIT_FAILURE);
               } else if (argv[i][j] == 'x') {
                  strcpy(specifiedSections[flagXs], argv[i+1]);
                  flagXs++;
                  i++;
                  break;
               }
               j++;
            }
         }
      } else if (argv[i][0] != '-'){
         strcpy(files[filescount], argv[i]);
         filescount++;
      }
   }

   //Check if no input files
   if (filescount == 0) {
      fprintf(stderr, "readelf: Warning: Nothing to do.\n");
      printUse();
      exit(EXIT_FAILURE);
   }
}

bool isNumeric(const char *str) {
   while(*str != '\0') {
      if(*str < '0' || *str > '9')
         return false;
      str++;
   }
   return true;
}

int checkSpecifiedSection(int n, uint16_t shnum) {
   int sectionIndex = -1;

   // Check if number
   if (isNumeric(specifiedSections[n])) {
      int specifiedIndex = atoi(specifiedSections[n]);
      // Check if index within range
      if (specifiedIndex > shnum - 1) {
         fprintf(stderr, "readelf: Warning: Section %d was not dumped because it does not exist!\n", specifiedIndex);
      } else {
         sectionIndex = specifiedIndex;
      }
   } else { // Check string match
      for (int i = 0; i < shnum; i++) {
         if (strcmp(sectionNames[i], specifiedSections[n]) == 0) {
            sectionIndex = i;
            break;
         }
      }
      if (sectionIndex == -1) {
         fprintf(stderr, "readelf: Warning: Section \'%s\' was not dumped because it does not exist!\n", specifiedSections[n]);
      }
   }
   return sectionIndex;
}

void printByFlags32(Elf32_Ehdr ehead, Elf32_Shdr sections[], FILE * fp) {
   if (flagNone) {
      printEHead32(ehead); printf("\n");
      printSHeads32(sections, sectionNames, ehead.e_shnum);
   } else {
      if (flagEH) {
         printEHead32(ehead);
      }
      if (flagSH && flagEH) {
         printf("\n");
         printSHeads32(sections, sectionNames, ehead.e_shnum);
      } 
      if (flagSH && !flagEH) {
         printf("There are %d section headers, starting at offset 0x%x:\n\n", ehead.e_shnum, ehead.e_shoff);
         printSHeads32(sections, sectionNames, ehead.e_shnum);
      }
      for (int i = 0; i < flagXs; i++) {
         int sectionIndex = checkSpecifiedSection(i, ehead.e_shnum);
         if (sectionIndex != -1) {
            printSection32(sections[sectionIndex], sectionNames[sectionIndex], fp);
         }
      }
   }
}

void printByFlags64(Elf64_Ehdr ehead, Elf64_Shdr sections[], FILE * fp) {
   if (flagNone) {
      printEHead64(ehead); printf("\n");
      printSHeads64(sections, sectionNames, ehead.e_shnum);
   } else {
      if (flagEH) {
         printEHead64(ehead);
      }
      if (flagSH && flagEH) {
         printf("\n");
         printSHeads64(sections, sectionNames, ehead.e_shnum);
      } 
      if (flagSH && !flagEH) {
         printf("There are %d section headers, starting at offset 0x%lx:\n\n", ehead.e_shnum, ehead.e_shoff);
         printSHeads64(sections, sectionNames, ehead.e_shnum);
      }
      for (int i = 0; i < flagXs; i++) {
         int sectionIndex = checkSpecifiedSection(i, ehead.e_shnum);
         if (sectionIndex != -1) {
            printSection64(sections[sectionIndex], sectionNames[sectionIndex], fp);
         }
      }
   }
}

void processAFile(FILE * fp) {
   // Read ELF Header
   Elf32_Ehdr ehead;
   fread(&ehead, sizeof(ehead), 1, fp);

   // Check if it an ELF file
   if (!isElf(ehead)) {
      fprintf(stderr, "readelf: Error, not an ELF file.\n");
      exit(EXIT_FAILURE);
   }
   
   if (!is64Bit(ehead)) {
      // Process 32 bit ELF File
      checkEndianness32(&ehead);
      
      // Read Section headers
      Elf32_Shdr sections[ehead.e_shnum];
      fseek(fp, ehead.e_shoff, SEEK_SET);
      fread(sections, ehead.e_shentsize, ehead.e_shnum, fp);
      fixSHeadsEndianness32(sections, ehead.e_shnum);
      
      // Read Section Names
      char * namesString = malloc(sections[ehead.e_shstrndx].sh_size);
      fseek(fp, sections[ehead.e_shstrndx].sh_offset, SEEK_SET);
      fread(namesString, sections[ehead.e_shstrndx].sh_size, 1, fp);
      for (int i = 0; i < ehead.e_shnum; i++) {
         strcpy(sectionNames[i], namesString + sections[i].sh_name);
      }
      free(namesString);

      // Print according to flags
      printByFlags32(ehead, sections, fp);


   } else {
      // Process 64 bit ELF File
      // Read ELF header
      rewind(fp);
      Elf64_Ehdr ehead;
      fread(&ehead, sizeof(ehead), 1, fp);
      checkEndianness64(&ehead);

      // Read Section headers
      Elf64_Shdr sections[ehead.e_shnum];
      fseek(fp, ehead.e_shoff, SEEK_SET);
      fread(sections, ehead.e_shentsize, ehead.e_shnum, fp);
      fixSHeadsEndianness64(sections, ehead.e_shnum);

      // Read Section Names
      char * namesString = malloc(sections[ehead.e_shstrndx].sh_size);
      fseek(fp, sections[ehead.e_shstrndx].sh_offset, SEEK_SET);
      fread(namesString, sections[ehead.e_shstrndx].sh_size, 1, fp);
      for (int i = 0; i < ehead.e_shnum; i++) {
         strcpy(sectionNames[i], namesString + sections[i].sh_name);
      }
      free(namesString);

      // Print according to flags
      printByFlags64(ehead, sections, fp);

   }
}

void processFiles() {
   for (int i=0; i<filescount; i++) {
      FILE *fp = fopen(files[i], "rb");
      if (fp == NULL) {
         fprintf(stderr, "readelf: Unable to open %s\n", files[i]);
         exit(EXIT_FAILURE);
      } else {
         if (filescount!= 1) printf("\nFile: %s\n", files[i]);
         processAFile(fp);
         fclose(fp);
      }
   }
}

int main(int argc, const char * argv[]) {
   
   checkInput(argc, argv);
   
   processFiles();
   
   exit(EXIT_SUCCESS);
}

