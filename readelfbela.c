///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 05 - readelf
///
/// @file readelfbela.c
/// @version 2.0
///
/// @author Mirabela Medallon <mirabela@hawaii.edu>
/// @brief  Lab 05 - readelf - EE 491F - Spr 2021
/// @date   March 5, 2021
///////////////////////////////////////////////////////////////////////////////

// NOTE: I did the extra credit.

#include "readelfbela.h"

bool sameEndianness;

bool isElf(Elf32_Ehdr ehead) {
   if(ehead.e_ident[EI_MAG0] != ELFMAG0) {
      fprintf(stderr, "ELF Header EI_MAG0 wrong.\n");
       return false;
   }
   if(ehead.e_ident[EI_MAG1] != ELFMAG1) {
      fprintf(stderr, "ELF Header EI_MAG1 wrong.\n");
       return false;
   }
   if(ehead.e_ident[EI_MAG2] != ELFMAG2) {
      fprintf(stderr, "ELF Header EI_MAG2 wrong.\n");
       return false;
   }
   if(ehead.e_ident[EI_MAG3] != ELFMAG3) {
      fprintf(stderr, "ELF Header EI_MAG3 wrong.\n");
      return false;
   }
   return true;
}

bool is64Bit(Elf32_Ehdr ehead) {
   if (ehead.e_ident[EI_CLASS] == ELFCLASS64)
      return true;
   else
      return false;
}

void checkEndianness32(Elf32_Ehdr * ehead) {
   sameEndianness = false;
   char machEnd = 0;
   char fileEnd = ehead->e_ident[EI_DATA];
   unsigned int check = 1;
   if (*(char *)&check == 1) {
      machEnd = 1;
   } else {
      machEnd = 2;
   }

   if (fileEnd == machEnd) {
      sameEndianness = true;
   }

   if (!sameEndianness) {
      bSwapEH32(ehead);
   }
}

void checkEndianness64(Elf64_Ehdr * ehead) {
   sameEndianness = false;
   char machEnd = 0;
   char fileEnd = ehead->e_ident[EI_DATA];
   unsigned int check = 1;
   if (*(char *)&check == 1) {
      machEnd = 1;
   } else {
      machEnd = 2;
   }

   if (fileEnd == machEnd) {
      sameEndianness = true;
   }
   
   if (!sameEndianness) {
      bSwapEH64(ehead);
   }
}

/**
 * Fixes endian errors: 32-bit ELF header
 */
void bSwapEH32 (Elf32_Ehdr * ehead) {
   ehead->e_type = bswap_16(ehead->e_type);
   ehead->e_machine = bswap_16(ehead->e_machine);
   ehead->e_version = bswap_32(ehead->e_version);
   ehead->e_entry = bswap_32(ehead->e_entry);
   ehead->e_phoff = bswap_32(ehead->e_phoff);
   ehead->e_shoff = bswap_32(ehead->e_shoff);
   ehead->e_flags = bswap_32(ehead->e_flags);
   ehead->e_ehsize = bswap_16(ehead->e_ehsize);
   ehead->e_phentsize = bswap_16(ehead->e_phentsize);
   ehead->e_phnum = bswap_16(ehead->e_phnum);
   ehead->e_shentsize = bswap_16(ehead->e_shentsize);
   ehead->e_shnum = bswap_16(ehead->e_shnum);
   ehead->e_shstrndx = bswap_16(ehead->e_shstrndx);
}

/**
 * Fixes endian errors: 64-bit ELF header
 */
void bSwapEH64 (Elf64_Ehdr * ehead) {
   ehead->e_type = bswap_16(ehead->e_type);
   ehead->e_machine = bswap_16(ehead->e_machine);
   ehead->e_version = bswap_32(ehead->e_version);
   ehead->e_entry = bswap_64(ehead->e_entry);
   ehead->e_phoff = bswap_64(ehead->e_phoff);
   ehead->e_shoff = bswap_64(ehead->e_shoff);
   ehead->e_flags = bswap_32(ehead->e_flags);
   ehead->e_ehsize = bswap_16(ehead->e_ehsize);
   ehead->e_phentsize = bswap_16(ehead->e_phentsize);
   ehead->e_phnum = bswap_16(ehead->e_phnum);
   ehead->e_shentsize = bswap_16(ehead->e_shentsize);
   ehead->e_shnum = bswap_16(ehead->e_shnum);
   ehead->e_shstrndx = bswap_16(ehead->e_shstrndx);
}

/**
 * Fixes endian errors: 32-bit Section header
 */
void bSwapSH32 (Elf32_Shdr * shead) {
   shead->sh_name = bswap_32(shead->sh_name);
   shead->sh_type = bswap_32(shead->sh_type);
   shead->sh_flags = bswap_32(shead->sh_flags);
   shead->sh_addr = bswap_32(shead->sh_addr);
   shead->sh_offset = bswap_32(shead->sh_offset);
   shead->sh_size = bswap_32(shead->sh_size);
   shead->sh_link = bswap_32(shead->sh_link);
   shead->sh_info = bswap_32(shead->sh_info);
   shead->sh_addralign = bswap_32(shead->sh_addralign);
   shead->sh_entsize = bswap_32(shead->sh_entsize);
}

/**
 * Fixes endian errors: 64-bit Section header
 */
void bSwapSH64 (Elf64_Shdr * shead) {
   shead->sh_name = bswap_32(shead->sh_name);
   shead->sh_type = bswap_32(shead->sh_type);
   shead->sh_flags = bswap_64(shead->sh_flags);
   shead->sh_addr = bswap_64(shead->sh_addr);
   shead->sh_offset = bswap_64(shead->sh_offset);
   shead->sh_size = bswap_64(shead->sh_size);
   shead->sh_link = bswap_32(shead->sh_link);
   shead->sh_info = bswap_32(shead->sh_info);
   shead->sh_addralign = bswap_64(shead->sh_addralign);
   shead->sh_entsize = bswap_64(shead->sh_entsize);
}

/**
 * Print 32 bit ELF header
 */
void printEHead32(Elf32_Ehdr ehead) {

   printf("ELF Header:\n");
   // Magic:
   printf("  Magic:   ");
   for (int i = 0; i < EI_NIDENT; i++) {
      printf("%02x ",ehead.e_ident[i]);
   }
   printf("\n");
   
   // Class:
   printf("  %-35s","Class:");
   switch(ehead.e_ident[EI_CLASS]) {
      case ELFCLASS32: printf("ELF32\n"); break;
      case ELFCLASS64: printf("ELF64\n"); break;
      default: printf("Invalid class\n"); break;
   }
   
   // Data:
   printf("  %-35s","Data:");
   switch(ehead.e_ident[EI_DATA]) {
      case ELFDATA2LSB: printf("2's complement, little endian\n"); break;
      case ELFDATA2MSB: printf("2's complement, big endian\n"); break;
      default: printf("Unknown data format\n"); break;
   }
   
   // Version:
   printf("  %-35s","Version:");
   switch(ehead.e_ident[EI_VERSION]) {
      case EV_CURRENT: printf("%d (current)\n", ehead.e_ident[EI_VERSION]); break;
      case EV_NONE: printf("Invalid version\n"); break;
      default: printf("%d\n", ehead.e_ident[EI_VERSION]); break;
   }
   
   // OS/ABI:
   printf("  %-35s","OS/ABI:");
   switch(ehead.e_ident[EI_OSABI]) {
      case ELFOSABI_SYSV:
         printf("UNIX System V\n");
         break;

      case ELFOSABI_HPUX:
         printf("HP-UX\n");
         break;

      case ELFOSABI_NETBSD:
         printf("NetBSD\n");
         break;

      case ELFOSABI_LINUX:
         printf("GNU/Linux\n");
         break;

      case ELFOSABI_SOLARIS:
         printf("Solaris\n");
         break;

      case ELFOSABI_AIX:
         printf("Monterey/AIX\n");
         break;

      case ELFOSABI_IRIX:
         printf("IRIX\n");
         break;

      case ELFOSABI_FREEBSD:
         printf("FreeBSD\n");
         break;

      case ELFOSABI_TRU64:
         printf("TRU64 UNIX\n");
         break;
         
      case ELFOSABI_ARM:
         printf("ARM architecture\n");
         break;

      case ELFOSABI_MODESTO:
         printf("Novell Modesto\n");
         break;

      case ELFOSABI_STANDALONE:
         printf("Standalone (embedded)\n");
         break;


      default:
         printf("Unknown (0x%x)\n", ehead.e_ident[EI_OSABI]);
         break;
   }
   
   // ABI Version:
   printf("  %-35s%x\n", "ABI Version:", ehead.e_ident[EI_ABIVERSION]);
   
   // Type:
   printf("  %-35s","Type:");
   switch(ehead.e_type) {
      case ET_NONE:
         printf("NONE (Unknown type)\n");
         break;

      case ET_REL:
         printf("REL (Relocatable)\n");
         break;

      case ET_EXEC:
         printf("EXEC (Executable)\n");
         break;

      case ET_DYN:
         printf("DYN (Shared Object)\n");
         break;
         
      case ET_CORE:
         printf("CORE (Core)\n");
         break;
         
      default:
         printf("Unknown (0x%x)\n", ehead.e_type);
         break;
   }
   
   // Machine:
   printf("  %-35s","Machine:");
   switch(ehead.e_machine) {
      case EM_NONE:
         printf("None (unknown machine)\n");
         break;

      case EM_M32:
         printf("M32 (AT&T WE 32100)\n");
         break;

      case EM_SPARC:
         printf("SPARC (Sun Microsystems SPARC)\n");
         break;
   
      case EM_386:
         printf("386 (Intel 80386)\n");
         break;
         
      case EM_68K:
         printf("68K (Motorola 68000)\n");
         break;

      case EM_88K:
         printf("88K (Motorola 88000)\n");
         break;

      case EM_860:
         printf("860 (Intel 80860)\n");
         break;

      case EM_MIPS:
         printf("MIPS (MIPS RS3000 (big-endian only))\n");
         break;

      case 0x16:
         printf("IBM S/390\n");
         break;

      case 0x3e:
         printf("Advanced Micro Devices X86-64\n");
         break;     

      case 0xb7:
         printf("AArch64\n");
         break;     

      case 0x15:
         printf("PowerPC64\n");
         break;     

      case 0x28:
         printf("ARM\n");
         break;     

      default:
         printf("0x%x\n", ehead.e_machine);
         break;
   }
   
   //  Version:
   printf("  %-35s","Version:");
   switch(ehead.e_version) {
      // case EV_CURRENT:
      //    printf("Current Version\n");
      //    break;

      // case EV_NONE:
      //    printf("Invalid version\n");
      //    break;

      default:
         printf("0x%x\n", ehead.e_version);
         break;
   }
   
   // Entry point address: 32
   printf("  %-35s0x%x\n", "Entry point address:", ehead.e_entry);
   
   // Start of program headers: 32
   printf("  %-35s%d (bytes into file)\n", "Start of program headers:", ehead.e_phoff);
   
   // Start of section headers: 32
   printf("  %-35s%d (bytes into file)\n", "Start of section headers:", ehead.e_shoff);

   // Flags:
   printf("  %-35s0x%x\n", "Flags:", ehead.e_flags);
   
    // Size of this header:
   printf("  %-35s%d (bytes)\n", "Size of this header:", ehead.e_ehsize);

   // Size of program headers:
   printf("  %-35s%d (bytes)\n", "Size of program headers:", ehead.e_phentsize);

   // Number of program headers:
   printf("  %-35s%d\n", "Number of program headers:", ehead.e_phnum);

   // Size of section headers:
   printf("  %-35s%d (bytes)\n", "Size of section headers:", ehead.e_shentsize);

   // Number of section headers:
   printf("  %-35s%d\n", "Number of section headers:", ehead.e_shnum);

   // Section header string table index:
   printf("  %-35s%d\n", "Section header string table index:", ehead.e_shstrndx);

}   

/**
 * Print 64 bit ELF header
 */
void printEHead64(Elf64_Ehdr ehead) {

   printf("ELF Header:\n");
   // Magic:
   printf("  Magic:   ");
   for (int i = 0; i < EI_NIDENT; i++) {
      printf("%02x ",ehead.e_ident[i]);
   }
   printf("\n");
   
   // Class:
   printf("  %-35s","Class:");
   switch(ehead.e_ident[EI_CLASS]) {
      case ELFCLASS32: printf("ELF32\n"); break;
      case ELFCLASS64: printf("ELF64\n"); break;
      default: printf("Invalid class\n"); break;
   }
   
   // Data:
   printf("  %-35s","Data:");
   switch(ehead.e_ident[EI_DATA]) {
      case ELFDATA2LSB: printf("2's complement, little endian\n"); break;
      case ELFDATA2MSB: printf("2's complement, big endian\n"); break;
      default: printf("Unknown data format\n"); break;
   }
    
   // Version:
   printf("  %-35s","Version:");
   switch(ehead.e_ident[EI_VERSION]) {
      case EV_CURRENT: printf("%d (current)\n", ehead.e_ident[EI_VERSION]); break;
      case EV_NONE: printf("Invalid version\n"); break;
      default: printf("%d\n", ehead.e_ident[EI_VERSION]); break;
   }
     
   // OS/ABI:
   printf("  %-35s","OS/ABI:");
   switch(ehead.e_ident[EI_OSABI]) {
      case ELFOSABI_SYSV:
         printf("UNIX System V\n");
         break;

      case ELFOSABI_HPUX:
         printf("HP-UX\n");
         break;

      case ELFOSABI_NETBSD:
         printf("NetBSD\n");
         break;

      case ELFOSABI_LINUX:
         printf("GNU/Linux\n");
         break;

      case ELFOSABI_SOLARIS:
         printf("Solaris\n");
         break;

      case ELFOSABI_AIX:
         printf("Monterey/AIX\n");
         break;

      case ELFOSABI_IRIX:
         printf("IRIX\n");
         break;

      case ELFOSABI_FREEBSD:
         printf("FreeBSD\n");
         break;

      case ELFOSABI_TRU64:
         printf("TRU64 UNIX\n");
         break;
         
      case ELFOSABI_ARM:
         printf("ARM architecture\n");
         break;

      case ELFOSABI_MODESTO:
         printf("Novell Modesto\n");
         break;

      case ELFOSABI_STANDALONE:
         printf("Standalone (embedded)\n");
         break;


      default:
         printf("Unknown (0x%x)\n", ehead.e_ident[EI_OSABI]);
         break;
   }
   
   // ABI Version:
   printf("  %-35s%x\n", "ABI Version:", ehead.e_ident[EI_ABIVERSION]);
   
   // Type:
   printf("  %-35s","Type:");
   switch(ehead.e_type) {
      case ET_NONE:
         printf("NONE (Unknown type)\n");
         break;

      case ET_REL:
         printf("REL (Relocatable)\n");
         break;

      case ET_EXEC:
         printf("EXEC (Executable)\n");
         break;

      case ET_DYN:
         printf("DYN (Shared Object)\n");
         break;
         
      case ET_CORE:
         printf("CORE (Core)\n");
         break;
         
      default:
         printf("Unknown (0x%x)\n", ehead.e_type);
         break;
   }
   
   // Machine:
   printf("  %-35s","Machine:");
   switch(ehead.e_machine) {
      case EM_NONE:
         printf("None (unknown machine)\n");
         break;

      case EM_M32:
         printf("M32 (AT&T WE 32100)\n");
         break;

      case EM_SPARC:
         printf("SPARC (Sun Microsystems SPARC)\n");
         break;
   
      case EM_386:
         printf("386 (Intel 80386)\n");
         break;
         
      case EM_68K:
         printf("68K (Motorola 68000)\n");
         break;

      case EM_88K:
         printf("88K (Motorola 88000)\n");
         break;

      case EM_860:
         printf("860 (Intel 80860)\n");
         break;

      case EM_MIPS:
         printf("MIPS (MIPS RS3000 (big-endian only))\n");
         break;

      case 0x16:
         printf("IBM S/390\n");
         break;

      case 0x3e:
         printf("Advanced Micro Devices X86-64\n");
         break;     

      case 0xb7:
         printf("AArch64\n");
         break;     

      case 0x15:
         printf("PowerPC64\n");
         break;     

      case 0x28:
         printf("ARM\n");
         break;     

      default:
         printf("0x%x\n", ehead.e_machine);
         break;
   }
   
   //  Version:
   printf("  %-35s","Version:");
   switch(ehead.e_version) {
      // case EV_CURRENT:
      //    printf("Current Version\n");
      //    break;

      // case EV_NONE:
      //    printf("Invalid version\n");
      //    break;

      default:
         printf("0x%x\n", ehead.e_version);
         break;
   }
   
   // Entry point address: 64
   printf("  %-35s0x%lx\n", "Entry point address:", ehead.e_entry);
   
   // Start of program headers: 64
   printf("  %-35s%ld (bytes into file)\n", "Start of program headers:", ehead.e_phoff);
   
   // Start of section headers: 64
   printf("  %-35s%ld (bytes into file)\n", "Start of section headers:", ehead.e_shoff);

   // Flags:
   printf("  %-35s0x%x\n", "Flags:", ehead.e_flags);
   
    // Size of this header:
   printf("  %-35s%d (bytes)\n", "Size of this header:", ehead.e_ehsize);

   // Size of program headers:
   printf("  %-35s%d (bytes)\n", "Size of program headers:", ehead.e_phentsize);

   // Number of program headers:
   printf("  %-35s%d\n", "Number of program headers:", ehead.e_phnum);

   // Size of section headers:
   printf("  %-35s%d (bytes)\n", "Size of section headers:", ehead.e_shentsize);

   // Number of section headers:
   printf("  %-35s%d\n", "Number of section headers:", ehead.e_shnum);

   // Section header string table index:
   printf("  %-35s%d\n", "Section header string table index:", ehead.e_shstrndx);

}   

void fixSHeadsEndianness32(Elf32_Shdr * sheads, uint16_t shnum) {
   if (!sameEndianness) {
      for (int i = 0; i < shnum; i++) {
         bSwapSH32(&sheads[i]);
      }
   }
}

void fixSHeadsEndianness64(Elf64_Shdr * sheads, uint16_t shnum) {
   if (!sameEndianness) {
      for (int i = 0; i < shnum; i++) {
         bSwapSH64(&sheads[i]);
      }
   }
}

void printSHeads32(Elf32_Shdr * sheads, char sectionNames[MAX_SECTIONS][MAX_SECTION_NAME_LEN], uint16_t shnum) {
   printf("Section Headers:\n  [Nr] Name\n");
   printf("       Type              Address          Offset            Link\n");
   printf("       Size              EntSize          Info              Align\n");
   printf("       Flags\n");

   for (int i = 0; i < shnum; i++) {
      // Print name
      printf("  [%2d] %s\n", i, sectionNames[i]);

      // Print Type
      char type[17] = "";
      switch (sheads[i].sh_type) {
         case SHT_NULL: strcpy(type, "NULL"); break;
         case SHT_PROGBITS: strcpy(type, "PROGBITS"); break;
         case SHT_SYMTAB: strcpy(type, "SYMTAB"); break;
         case SHT_STRTAB: strcpy(type, "STRTAB"); break;
         case SHT_RELA: strcpy(type, "RELA"); break;
         case SHT_HASH: strcpy(type, "HASH"); break;
         case SHT_DYNAMIC: strcpy(type, "DYNAMIC"); break;
         case SHT_NOTE: strcpy(type, "NOTE"); break;
         case SHT_NOBITS: strcpy(type, "NOBITS"); break;
         case SHT_REL: strcpy(type, "REL"); break;
         case SHT_SHLIB: strcpy(type, "SHLIB"); break;
         case SHT_DYNSYM: strcpy(type, "DYNSYM"); break;
         case SHT_INIT_ARRAY: strcpy(type, "INIT_ARRAY"); break;
         case SHT_FINI_ARRAY: strcpy(type, "FINI_ARRAY"); break;
         case SHT_PREINIT_ARRAY: strcpy(type, "PREINIT_ARRAY"); break;
         case SHT_GROUP: strcpy(type, "GROUP"); break;
         case SHT_SYMTAB_SHNDX: strcpy(type, "SYMTAB_SHNDX"); break;
         case SHT_LOOS: strcpy(type, "LOOS"); break;
         case SHT_LOSUNW: strcpy(type, "LOSUNW"); break;
         case SHT_SUNW_cap: strcpy(type, "SUNW_cap"); break;
         case SHT_GNU_HASH: strcpy(type, "GNU_HASH"); break;
         case SHT_GNU_LIBLIST: strcpy(type, "GNU_LIBLIST"); break;
         case SHT_SUNW_DEBUGSTR: strcpy(type, "SUNW_DEBUGSTR"); break;
         case SHT_SUNW_DEBUG: strcpy(type, "SUNW_DEBUG"); break;
         case SHT_SUNW_move: strcpy(type, "SUNW_move"); break;
         case SHT_SUNW_COMDAT: strcpy(type, "SUNW_COMDAT"); break;
         case SHT_SUNW_syminfo: strcpy(type, "SUNW_syminfo"); break;
         case SHT_GNU_verdef: strcpy(type, "GNU_verdef"); break;
         case SHT_GNU_verneed: strcpy(type, "GNU_verneed"); break;
         case SHT_GNU_versym: strcpy(type, "GNU_versym"); break;
         case SHT_LOPROC: strcpy(type, "LOPROC"); break;
         case SHT_AMD64_UNWIND: strcpy(type, "UNWIND"); break;
         case SHT_ARM_PREEMPTMAP: strcpy(type, "PREEMPTMAP"); break;
         case SHT_ARM_ATTRIBUTES: strcpy(type, "ATTRIBUTES"); break;
         case SHT_ARM_DEBUGOVERLAY: strcpy(type, "DEBUGOVERLAY"); break;
         case SHT_ARM_OVERLAYSECTION: strcpy(type, "OVERLAYSECTION"); break;
         case SHT_MIPS_REGINFO: strcpy(type, "REGINFO"); break;
         case SHT_MIPS_DWARF: strcpy(type, "DWARF"); break;
         case SHT_MIPS_OPTIONS: strcpy(type, "OPTIONS"); break;
         case SHT_HIPROC: strcpy(type, "HIPROC"); break;
         case SHT_LOUSER: strcpy(type, "LOUSER"); break;
         case SHT_HIUSER: strcpy(type, "HIUSER"); break;
         default: break;
      }
      printf("       %-16s", type);

      // Print Address
      printf(" %016x", sheads[i].sh_addr);

      // Print Offset
      printf("  %016x", sheads[i].sh_offset);

      // Print Link
      printf("  %d\n", sheads[i].sh_link);

      // Print Size
      printf("       %016x", sheads[i].sh_size);

      // Print EntSize
      printf(" %016x", sheads[i].sh_entsize);

      // Print Link
      printf("  %-16d", sheads[i].sh_info);

      // Print Align
      printf("  %d\n", sheads[i].sh_addralign);

      // Print Size
      printf("       [%016x]\n", sheads[i].sh_flags);

   }

}

void printSHeads64(Elf64_Shdr * sheads, char sectionNames[MAX_SECTIONS][MAX_SECTION_NAME_LEN], uint16_t shnum) {
   printf("Section Headers:\n  [Nr] Name\n");
   printf("       Type              Address          Offset            Link\n");
   printf("       Size              EntSize          Info              Align\n");
   printf("       Flags\n");

   for (int i = 0; i < shnum; i++) {
      // Print name
      printf("  [%2d] %s\n", i, sectionNames[i]);

      // Print Type
      char type[17] = "";
      switch (sheads[i].sh_type) {
         case SHT_NULL: strcpy(type, "NULL"); break;
         case SHT_PROGBITS: strcpy(type, "PROGBITS"); break;
         case SHT_SYMTAB: strcpy(type, "SYMTAB"); break;
         case SHT_STRTAB: strcpy(type, "STRTAB"); break;
         case SHT_RELA: strcpy(type, "RELA"); break;
         case SHT_HASH: strcpy(type, "HASH"); break;
         case SHT_DYNAMIC: strcpy(type, "DYNAMIC"); break;
         case SHT_NOTE: strcpy(type, "NOTE"); break;
         case SHT_NOBITS: strcpy(type, "NOBITS"); break;
         case SHT_REL: strcpy(type, "REL"); break;
         case SHT_SHLIB: strcpy(type, "SHLIB"); break;
         case SHT_DYNSYM: strcpy(type, "DYNSYM"); break;
         case SHT_INIT_ARRAY: strcpy(type, "INIT_ARRAY"); break;
         case SHT_FINI_ARRAY: strcpy(type, "FINI_ARRAY"); break;
         case SHT_PREINIT_ARRAY: strcpy(type, "PREINIT_ARRAY"); break;
         case SHT_GROUP: strcpy(type, "GROUP"); break;
         case SHT_SYMTAB_SHNDX: strcpy(type, "SYMTAB_SHNDX"); break;
         case SHT_LOOS: strcpy(type, "LOOS"); break;
         case SHT_LOSUNW: strcpy(type, "LOSUNW"); break;
         case SHT_SUNW_cap: strcpy(type, "SUNW_cap"); break;
         case SHT_GNU_HASH: strcpy(type, "GNU_HASH"); break;
         case SHT_GNU_LIBLIST: strcpy(type, "GNU_LIBLIST"); break;
         case SHT_SUNW_DEBUGSTR: strcpy(type, "SUNW_DEBUGSTR"); break;
         case SHT_SUNW_DEBUG: strcpy(type, "SUNW_DEBUG"); break;
         case SHT_SUNW_move: strcpy(type, "SUNW_move"); break;
         case SHT_SUNW_COMDAT: strcpy(type, "SUNW_COMDAT"); break;
         case SHT_SUNW_syminfo: strcpy(type, "SUNW_syminfo"); break;
         case SHT_GNU_verdef: strcpy(type, "GNU_verdef"); break;
         case SHT_GNU_verneed: strcpy(type, "GNU_verneed"); break;
         case SHT_GNU_versym: strcpy(type, "GNU_versym"); break;
         case SHT_LOPROC: strcpy(type, "LOPROC"); break;
         case SHT_AMD64_UNWIND: strcpy(type, "UNWIND"); break;
         case SHT_ARM_PREEMPTMAP: strcpy(type, "PREEMPTMAP"); break;
         case SHT_ARM_ATTRIBUTES: strcpy(type, "ATTRIBUTES"); break;
         case SHT_ARM_DEBUGOVERLAY: strcpy(type, "DEBUGOVERLAY"); break;
         case SHT_ARM_OVERLAYSECTION: strcpy(type, "OVERLAYSECTION"); break;
         case SHT_MIPS_REGINFO: strcpy(type, "REGINFO"); break;
         case SHT_MIPS_DWARF: strcpy(type, "DWARF"); break;
         case SHT_MIPS_OPTIONS: strcpy(type, "OPTIONS"); break;
         case SHT_HIPROC: strcpy(type, "HIPROC"); break;
         case SHT_LOUSER: strcpy(type, "LOUSER"); break;
         case SHT_HIUSER: strcpy(type, "HIUSER"); break;
         default: break;
      }
      printf("       %-16s", type);

      // Print Address
      printf(" %016lx", sheads[i].sh_addr);

      // Print Offset
      printf("  %016lx", sheads[i].sh_offset);

      // Print Link
      printf("  %d\n", sheads[i].sh_link);

      // Print Size
      printf("       %016lx", sheads[i].sh_size);

      // Print EntSize
      printf(" %016lx", sheads[i].sh_entsize);

      // Print Link
      printf("  %-16d", sheads[i].sh_info);

      // Print Align
      printf("  %ld\n", sheads[i].sh_addralign);

      // Print Size
      printf("       [%016lx]\n", sheads[i].sh_flags);
   }
}

void printSection32(Elf32_Shdr shead, char sectionName[MAX_SECTION_NAME_LEN], FILE * fp) {
   printf("\n");

   // Hex Dump
   printf("Hex dump of section '%s':\n", sectionName);
   
   unsigned char buf[16];
   uint32_t addr, offset, totalBytes, bytesRead, x, size;
   addr = shead.sh_addr;
   offset = shead.sh_offset;
   size = shead.sh_size;
   totalBytes = 0;

   fseek(fp, offset, SEEK_SET);

	while(totalBytes < size+1) {
		bytesRead = fread(buf,sizeof(unsigned char),16,fp);
      totalBytes += bytesRead;
		if(bytesRead==0) break;
      
      if (totalBytes > size+1) {
         bytesRead-= totalBytes - size;
      }
		if(bytesRead==0) break;

		// Print Address
		printf("  0x%08x ",addr);

		// Print hex
		for( x=0; x<bytesRead; x++ ) {
			printf("%02x",buf[x]);
         if (x == 3 || x == 7 || x == 11) putchar(' ');
		}
		
      // Fill in blank row
		if( bytesRead<16 ) {
			for( x=bytesRead; x<16; x++ ) {
				printf("  ");
            if (x == 3 || x == 7 || x == 11) putchar(' ');
			}
		}

		// Print ASCII
		printf(" ");
		for( x=0; x<bytesRead; x++) {
			if( buf[x]<32 || buf[x]>126 ) putchar('.');
			else putchar(buf[x]);
		}
		putchar('\n');
		addr += bytesRead;

	}

   printf("\n");

}

void printSection64(Elf64_Shdr shead, char sectionName[MAX_SECTION_NAME_LEN], FILE * fp) {
   printf("\n");

   // Hex Dump
   printf("Hex dump of section '%s':\n", sectionName);
   unsigned char buf[16];
   uint64_t addr, offset, totalBytes, bytesRead, x, size;
   addr = shead.sh_addr;
   offset = shead.sh_offset;
   size = shead.sh_size;
   totalBytes = 0;

   fseek(fp, offset, SEEK_SET);
	while(totalBytes < size+1) {
		bytesRead = fread(buf,sizeof(unsigned char),16,fp);
      totalBytes += bytesRead;
		if(bytesRead==0) break;
      
      if (totalBytes > size+1) {
         bytesRead-= totalBytes - size;
      }
		if(bytesRead==0) break;

		// Print Address
		printf("  0x%08lx ",addr);

		// Print hex
		for( x=0; x<bytesRead; x++ ) {
			printf("%02x",buf[x]);
         if (x == 3 || x == 7 || x == 11) putchar(' ');
		}
		
      // Fill in blank row
		if( bytesRead<16 ) {
			for( x=bytesRead; x<16; x++ ) {
				printf("  ");
            if (x == 3 || x == 7 || x == 11) putchar(' ');
			}
		}

		// Print ASCII
		printf(" ");
		for( x=0; x<bytesRead; x++) {
			if( buf[x]<32 || buf[x]>126 ) putchar('.');
			else putchar(buf[x]);
		}
		putchar('\n');
		addr += bytesRead;

	}
   printf("\n");

}


