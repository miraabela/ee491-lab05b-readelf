///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 05 - readelf
///
/// @file readelfbela.h
/// @version 2.0
///
/// @author Mirabela Medallon <mirabela@hawaii.edu>
/// @brief  Lab 05 - readelf - EE 491F - Spr 2021
/// @date   March 5, 2021
///////////////////////////////////////////////////////////////////////////////

#ifndef readelfbela_h
#define readelfbela_h

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>
#include <stdint.h>
#include <byteswap.h>

#define MAX_SECTIONS 128
#define MAX_SECTION_NAME_LEN 128


// Defines and structs taken from ELF header file

#define EI_MAG0      0  
#define EI_MAG1      1 
#define EI_MAG2      2  
#define EI_MAG3      3 
#define EI_CLASS     4 
#define EI_DATA      5 
#define EI_VERSION   6
#define EI_OSABI     7  
#define EI_ABIVERSION 8 
#define OLD_EI_BRAND 8  
#define EI_PAD       9 
#define EI_NIDENT    16

#define ELFMAG0      0x7f
#define ELFMAG1      'E'
#define ELFMAG2      'L'
#define ELFMAG3      'F'

#define EV_NONE      0
#define EV_CURRENT   1

#define ELFCLASSNONE 0 
#define ELFCLASS32   1 
#define ELFCLASS64   2

#define ELFDATANONE  0  /* Unknown data format. */
#define ELFDATA2LSB  1  /* 2's complement little-endian. */
#define ELFDATA2MSB  2  /* 2's complement big-endian. */

#define	ELFOSABI_NONE		0	/* UNIX System V ABI */
#define	ELFOSABI_HPUX		1	/* HP-UX operating system */
#define	ELFOSABI_NETBSD		2	/* NetBSD */
#define	ELFOSABI_LINUX		3	/* GNU/Linux */
#define	ELFOSABI_SOLARIS	6	/* Solaris */
#define	ELFOSABI_AIX		7	/* AIX */
#define	ELFOSABI_IRIX		8	/* IRIX */
#define	ELFOSABI_FREEBSD	9	/* FreeBSD */
#define	ELFOSABI_TRU64		10	/* TRU64 UNIX */
#define	ELFOSABI_MODESTO	11	/* Novell Modesto */
#define	ELFOSABI_ARM		97	/* ARM */
#define	ELFOSABI_STANDALONE	255	/* Standalone (embedded) application */
#define	ELFOSABI_SYSV		ELFOSABI_NONE	/* symbol used in old spec */
#define	ELFOSABI_MONTEREY	ELFOSABI_AIX	/* Monterey */

#define ET_NONE   0  /* Unknown type. */
#define ET_REL    1  /* Relocatable. */
#define ET_EXEC   2  /* Executable. */
#define ET_DYN    3  /* Shared object. */
#define ET_CORE   4  /* Core file. */

#define EM_NONE   0  /* Unknown machine. */
#define EM_M32    1  /* AT&T WE32100. */
#define EM_SPARC  2  /* Sun SPARC. */
#define EM_386    3  /* Intel i386. */
#define EM_68K    4  /* Motorola 68000. */
#define EM_88K    5  /* Motorola 88000. */
#define EM_486    6  /* Intel i486. */
#define EM_860    7  /* Intel i860. */
#define EM_MIPS      8  /* MIPS R3000 Big-Endian only */

#define	SHT_NULL		0	/* inactive */
#define	SHT_PROGBITS		1	/* program defined information */
#define	SHT_SYMTAB		2	/* symbol table section */
#define	SHT_STRTAB		3	/* string table section */
#define	SHT_RELA		4	/* relocation section with addends */
#define	SHT_HASH		5	/* symbol hash table section */
#define	SHT_DYNAMIC		6	/* dynamic section */
#define	SHT_NOTE		7	/* note section */
#define	SHT_NOBITS		8	/* no space section */
#define	SHT_REL			9	/* relocation section - no addends */
#define	SHT_SHLIB		10	/* reserved - purpose unknown */
#define	SHT_DYNSYM		11	/* dynamic symbol table section */
#define	SHT_INIT_ARRAY		14	/* Initialization function pointers. */
#define	SHT_FINI_ARRAY		15	/* Termination function pointers. */
#define	SHT_PREINIT_ARRAY	16	/* Pre-initialization function ptrs. */
#define	SHT_GROUP		17	/* Section group. */
#define	SHT_SYMTAB_SHNDX	18	/* Section indexes (see SHN_XINDEX). */
#define	SHT_LOOS		0x60000000	/* First of OS specific semantics */
#define	SHT_LOSUNW		0x6ffffff4
#define	SHT_SUNW_dof		0x6ffffff4
#define	SHT_SUNW_cap		0x6ffffff5
#define	SHT_SUNW_SIGNATURE	0x6ffffff6
#define	SHT_GNU_HASH		0x6ffffff6
#define	SHT_GNU_LIBLIST		0x6ffffff7
#define	SHT_SUNW_ANNOTATE	0x6ffffff7
#define	SHT_SUNW_DEBUGSTR	0x6ffffff8
#define	SHT_SUNW_DEBUG		0x6ffffff9
#define	SHT_SUNW_move		0x6ffffffa
#define	SHT_SUNW_COMDAT		0x6ffffffb
#define	SHT_SUNW_syminfo	0x6ffffffc
#define	SHT_SUNW_verdef		0x6ffffffd
#define	SHT_GNU_verdef		0x6ffffffd	/* Symbol versions provided */
#define	SHT_SUNW_verneed	0x6ffffffe
#define	SHT_GNU_verneed		0x6ffffffe	/* Symbol versions required */
#define	SHT_SUNW_versym		0x6fffffff
#define	SHT_GNU_versym		0x6fffffff	/* Symbol version table */
#define	SHT_HISUNW		0x6fffffff
#define	SHT_HIOS		0x6fffffff	/* Last of OS specific semantics */
#define	SHT_LOPROC		0x70000000	/* reserved range for processor */
#define	SHT_AMD64_UNWIND	0x70000001	/* unwind information */
#define	SHT_ARM_EXIDX		0x70000001	/* Exception index table. */
#define	SHT_ARM_PREEMPTMAP	0x70000002	/* BPABI DLL dynamic linking */
#define	SHT_ARM_ATTRIBUTES	0x70000003	/* Object file compatibility */
#define	SHT_ARM_DEBUGOVERLAY	0x70000004	/* See DBGOVL for details. */
#define	SHT_ARM_OVERLAYSECTION	0x70000005	/* See DBGOVL for details. */
#define	SHT_MIPS_REGINFO	0x70000006
#define	SHT_MIPS_OPTIONS	0x7000000d
#define	SHT_MIPS_DWARF		0x7000001e	/* MIPS gcc uses MIPS_DWARF */
#define	SHT_HIPROC		0x7fffffff	/* specific section header types */
#define	SHT_LOUSER		0x80000000	/* reserved range for application */
#define	SHT_HIUSER		0xffffffff	/* specific indexes */


typedef struct {
	unsigned char	e_ident[EI_NIDENT];	
	uint16_t	e_type;		
	uint16_t	e_machine;	
	uint32_t	e_version;	
	uint32_t	e_entry;	// Different than 64-bit
	uint32_t	e_phoff;	// Different than 64-bit
	uint32_t	e_shoff;	// Different than 64-bit
	uint32_t	e_flags;	
	uint16_t	e_ehsize;	
	uint16_t	e_phentsize;
	uint16_t	e_phnum;
	uint16_t	e_shentsize;
	uint16_t	e_shnum;
	uint16_t	e_shstrndx;
} Elf32_Ehdr;

typedef struct {
	unsigned char	e_ident[EI_NIDENT];	
	uint16_t	e_type;		
	uint16_t	e_machine;
	uint32_t	e_version;	
	uint64_t	e_entry;	// Different than 32-bit
	uint64_t	e_phoff;	// Different than 32-bit
	uint64_t	e_shoff;	// Different than 32-bit
	uint32_t	e_flags;	
	uint16_t	e_ehsize;	
	uint16_t	e_phentsize;
	uint16_t	e_phnum;	
	uint16_t	e_shentsize;	
	uint16_t	e_shnum;	
	uint16_t	e_shstrndx;	
} Elf64_Ehdr;

typedef struct {
	uint32_t	sh_name;
	uint32_t	sh_type;
	uint32_t	sh_flags;	// Different than 64-bit
	uint32_t	sh_addr;	// Different than 64-bit
	uint32_t	sh_offset;// Different than 64-bit
	uint32_t	sh_size;	// Different than 64-bit
	uint32_t	sh_link;	
	uint32_t	sh_info;
	uint32_t	sh_addralign;// Different than 64-bit
	uint32_t	sh_entsize;// Different than 64-bit
} Elf32_Shdr;

typedef struct {
	uint32_t	sh_name;	
	uint32_t	sh_type;	
	uint64_t	sh_flags;	// Different than 32-bit
	uint64_t	sh_addr; // Different than 32-bit
	uint64_t	sh_offset;	 // Different than 32-bit
	uint64_t	sh_size;	// Different than 32-bit
	uint32_t	sh_link;	
	uint32_t	sh_info;	
	uint64_t	sh_addralign;	// Different than 32-bit
	uint64_t	sh_entsize; // Different than 32-bit
} Elf64_Shdr;

bool isElf(Elf32_Ehdr ehead);
bool is64Bit(Elf32_Ehdr eh);
void checkEndianness32(Elf32_Ehdr * ehead);
void checkEndianness64(Elf64_Ehdr * ehead);
void bSwapEH32 (Elf32_Ehdr * ehead);
void bSwapEH64 (Elf64_Ehdr * ehead);
void bSwapSH32 (Elf32_Shdr * shead);
void bSwapSH64 (Elf64_Shdr * shead);
void printEHead32(Elf32_Ehdr ehead);
void printEHead64(Elf64_Ehdr ehead);
void fixSHeadsEndianness32(Elf32_Shdr * sheads, uint16_t shnum);
void fixSHeadsEndianness64(Elf64_Shdr * sheads, uint16_t shnum);
void printSHeads32(Elf32_Shdr * sheads, char sectionNames[MAX_SECTIONS][MAX_SECTION_NAME_LEN], uint16_t shnum);
void printSHeads64(Elf64_Shdr * sheads, char sectionNames[MAX_SECTIONS][MAX_SECTION_NAME_LEN], uint16_t shnum);
void printSection32(Elf32_Shdr shead, char sectionName[MAX_SECTION_NAME_LEN], FILE * fp);
void printSection64(Elf64_Shdr shead, char sectionName[MAX_SECTION_NAME_LEN], FILE * fp);


#endif /* readelfbela_h */

