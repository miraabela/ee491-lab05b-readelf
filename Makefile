CC = gcc
CFLAGS = -Wall
LDFLAGS =
OBJFILES = main.o readelfbela.o
TARGET = readelf
all: $(TARGET)

$(TARGET): $(OBJFILES)
	$(CC) $(CFLAGS) -o $(TARGET) $(OBJFILES) $(LDFLAGS)

test:
	./$(TARGET) -h test2 test5 test7
	./$(TARGET) -t test2 test5 test7
	./$(TARGET) -x 1 -x 2 test2 test5 test7


clean:
	rm -f $(OBJFILES) $(TARGET) *~


